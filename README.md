# git-test-project



## Git Project - Test

**Getting started with GitLab**

1. Create a **repository** with a **master** branch 
2. Commit dummy code into the **master** branch
3. Create two feature branch - **branch_1** and **branch_2**

## Access Git repository - Test

```
cd existing_repo
git remote add origin https://gitlab.com/sai.j.mihir/git-test-project.git
git branch -M main
git push -uf origin main
```

## Git Operations - Test
1. Merge **branch_1** to the **master** branch **using-merge** command 
2. Merge **branch_2** to the **master** branch **using-rebase** command 
3. Note the difference between both those commands and changes noticed.
